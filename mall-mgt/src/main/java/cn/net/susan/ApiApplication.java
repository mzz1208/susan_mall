package cn.net.susan;

import cn.net.susan.enable.EnableLimit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/3 下午3:44
 */
@EnableLimit
@EnableCaching
@SpringBootApplication(scanBasePackages = {"cn.net.susan"})
public class ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
    }
}
