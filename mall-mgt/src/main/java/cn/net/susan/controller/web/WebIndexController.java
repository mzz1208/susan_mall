package cn.net.susan.controller.web;

import cn.net.susan.annotation.NoLogin;
import cn.net.susan.entity.mall.IndexCarouselImageEntity;
import cn.net.susan.entity.mall.IndexProductEntity;
import cn.net.susan.service.mall.IndexCarouselImageService;
import cn.net.susan.service.mall.IndexProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * web首页操作controller
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/8/22 上午10:40
 */
@Api(tags = "web首页操作", description = "web首页操作")
@RestController
@RequestMapping("/v1/web/index")
public class WebIndexController {

    @Autowired
    private IndexCarouselImageService indexCarouselImageService;
    @Autowired
    private IndexProductService indexProductService;

    /**
     * 获取首页轮播图列表
     *
     * @return 首页轮播图列表
     */
    @NoLogin
    @GetMapping("/getIndexCarouselImageList")
    public List<IndexCarouselImageEntity> getIndexCarouselImageList() {
        return indexCarouselImageService.getIndexCarouselImageList();
    }

    /**
     * 获取首页商品列表
     *
     * @return 首页商品列表
     */
    @NoLogin
    @GetMapping("/getIndexProductList")
    public List<IndexProductEntity> getIndexProductList(@RequestParam("type") int type) {
        return indexProductService.getIndexProductList(type);
    }
}
