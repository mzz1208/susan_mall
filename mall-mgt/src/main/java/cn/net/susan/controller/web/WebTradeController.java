package cn.net.susan.controller.web;

import cn.net.susan.annotation.NoLogin;
import cn.net.susan.annotation.RepeatSubmit;
import cn.net.susan.annotation.VerifySign;
import cn.net.susan.entity.order.TradeEntity;
import cn.net.susan.entity.seckill.UserSeckillProductTradeEntity;
import cn.net.susan.service.order.TradeService;
import cn.net.susan.util.FillUserUtil;
import cn.net.susan.util.OrderCodeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 订单 接口层
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-05-30 15:33:03
 */
@Validated
@Api(tags = "订单操作", description = "订单接口")
@RestController
@RequestMapping("/v1/web/trade")
public class WebTradeController {

    @Autowired
    private TradeService tradeService;


    /**
     * 生成订单code
     *
     * @return 订单code
     */
    @ApiOperation(notes = "生成订单code", value = "生成订单code")
    @GetMapping("/generateCode")
    public TradeEntity generateCode() {
        TradeEntity tradeEntity = new TradeEntity();
        tradeEntity.setCode(OrderCodeUtil.generateOrderCode());
        return tradeEntity;
    }


    /**
     * 用户下单
     *
     * @param tradeEntity 订单实体
     * @return 影响行数
     */
    @RepeatSubmit
    @ApiOperation(notes = "用户下单", value = "用户下单")
    @PostMapping("/create")
    public TradeEntity create(@RequestBody @Valid TradeEntity tradeEntity) {
        tradeService.create(tradeEntity);
        return tradeEntity;
    }

    /**
     * 创建秒杀订单
     *
     * @param userSeckillProductTradeEntity 订单实体
     * @return 秒杀订单
     */
    @VerifySign
    @NoLogin
    @ApiOperation(notes = "创建秒杀订单", value = "创建秒杀订单")
    @PostMapping("/createForSeckill")
    public UserSeckillProductTradeEntity createForSeckill(@RequestBody @Valid UserSeckillProductTradeEntity userSeckillProductTradeEntity) {
        return tradeService.createUserSeckillProductTrade(userSeckillProductTradeEntity);
    }
}
