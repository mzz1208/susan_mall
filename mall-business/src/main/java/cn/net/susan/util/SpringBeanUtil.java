package cn.net.susan.util;

/**
 * Spring工具类
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/12 下午1:38
 */
public class SpringBeanUtil {

    /**
     * 根据名称获取bean实例
     *
     * @param name 名称
     * @param <T>  泛型
     * @return bean实例
     */
    public static <T> T getBean(String name) {
        return (T) SpringUtil.getBean(name);
    }

    /**
     * 根据类型获取bean实例
     *
     * @param requiredType 类型
     * @param <T>          泛型
     * @return bean实例
     */
    public static <T> T getBean(Class<T> requiredType) {
        return SpringUtil.getBean(requiredType);
    }
}
