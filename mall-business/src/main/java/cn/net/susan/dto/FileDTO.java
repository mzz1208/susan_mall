package cn.net.susan.dto;

import lombok.Data;

/**
 * 文件对象
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/5 下午5:21
 */
@Data
public class FileDTO {

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 下载地址
     */
    private String downloadUrl;
}
