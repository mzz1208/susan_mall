package cn.net.susan.mybatis;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;

import static cn.hutool.core.util.StrUtil.UNDERLINE;

/**
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/27 下午6:43
 */
public class DictCacheKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        return target.getClass().getSimpleName() + UNDERLINE + StringUtils.arrayToDelimitedString(params, UNDERLINE);
    }
}
