package cn.net.susan.enable;

import cn.net.susan.config.LimitConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分布式限流功能开关
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/4/28 上午10:32
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({LimitConfig.class})
public @interface EnableLimit {
}
