package cn.net.susan.config;

import cn.net.susan.util.SnowFlakeIdWorker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 分布式ID配置
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/21 下午6:37
 */
@Configuration
public class IdGenerateConfig {

    @Bean
    public SnowFlakeIdWorker snowFlakeIdWorker() {
        return new SnowFlakeIdWorker(0, 0);
    }
}
