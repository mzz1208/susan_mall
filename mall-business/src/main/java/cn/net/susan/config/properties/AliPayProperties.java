package cn.net.susan.config.properties;

import lombok.Data;

/**
 * 支付宝支付配置
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/6/12 下午5:39
 */
@Data
public class AliPayProperties {

    private String protocol;
    private String gatewayHost;
    private String signType;
    private String appId;
    private String privateKey;
    private String publicKey;
    private String notifyUrl;
}
