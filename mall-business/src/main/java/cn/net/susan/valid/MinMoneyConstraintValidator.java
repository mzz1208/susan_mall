package cn.net.susan.valid;

import cn.net.susan.annotation.MinMoney;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

/**
 * 最小金额校验
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/6/11 下午4:13
 */
public class MinMoneyConstraintValidator implements ConstraintValidator<MinMoney, BigDecimal> {

    private MinMoney constraint;

    @Override
    public void initialize(MinMoney constraint) {
        this.constraint = constraint;
    }

    @Override
    public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
        return value != null && value.doubleValue() >= constraint.value();
    }

}
