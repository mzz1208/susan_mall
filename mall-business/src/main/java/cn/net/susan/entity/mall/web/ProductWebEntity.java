package cn.net.susan.entity.mall.web;

import cn.net.susan.entity.EsBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 商品web实体
 *
 * @author 苏三
 * @date 2024/8/15 下午8:57
 */
@ApiModel("商品web实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductWebEntity extends EsBaseEntity {

    /**
     * 商品名称
     */
    @ApiModelProperty("商品名称")
    private String name;

    /**
     * 规格
     */
    @ApiModelProperty("规格")
    private String model;

    /**
     * 数量
     */
    @ApiModelProperty("数量")
    private Integer quantity;

    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private BigDecimal price;

    /**
     * 封面图片
     */
    @ApiModelProperty("封面图片")
    private String cover;

    /**
     * 商品类型
     */
    @ApiModelProperty("商品类型")
    private Integer productType;
}
