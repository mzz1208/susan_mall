package cn.net.susan.entity.mall;

import cn.net.susan.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商品详情实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-07-07 15:14:11
 */
@ApiModel("商品详情实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductDetailEntity extends BaseEntity {


	/**
	 * 商品ID
	 */
	@ApiModelProperty("商品ID")
	private Long productId;

	/**
	 * 商品详情
	 */
	@ApiModelProperty("商品详情")
	private String detail;
}
