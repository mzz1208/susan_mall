package cn.net.susan.entity.shopping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.net.susan.entity.BaseEntity;

/**
 * 商品评论实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-08-31 15:50:38
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductCommentEntity extends BaseEntity {


    /**
     * 父评论ID
     */
    private Long parentId;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 评分
     */
    private Integer rating;

    /**
     * 评论类型
     */
    private Integer type;
}
