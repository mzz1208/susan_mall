package cn.net.susan.entity.sys;

import cn.net.susan.annotation.Sensitive;
import cn.net.susan.entity.BaseEntity;
import cn.net.susan.enums.SensitiveTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;

/**
 * 用户实体 该项目是知识星球：java突击队 的内部项目
 *
 * @author 苏三
 * @date 2024-01-08 17:18:18
 */
@ApiModel("用户实体")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserEntity extends BaseEntity {


    /**
     * 头像
     */
    @ApiModelProperty("头像")
    private Long avatarId;

    /**
     * 邮箱
     */
    @NotEmpty(message = "邮箱不能为空")
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    @ApiModelProperty("用户名")
    private String userName;

    /**
     * 部门ID
     */
    @ApiModelProperty("部门ID")
    private Long deptId;

    /**
     * 部门
     */
    @ApiModelProperty("部门")
    private DeptEntity dept;

    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String deptName;

    /**
     * 手机号码
     */
    @Sensitive(type = SensitiveTypeEnum.MOBILE)
    @ApiModelProperty("手机号码")
    private String phone;

    /**
     * 岗位ID
     */
    @ApiModelProperty("岗位ID")
    private Long jobId;

    /**
     * 岗位
     */
    @ApiModelProperty("岗位")
    private List<JobEntity> jobs;

    /**
     * 最后修改密码的日期
     */
    @ApiModelProperty("最后修改密码的日期")
    private Date lastChangePasswordTime;

    /**
     * 别名
     */
    @ApiModelProperty("别名")
    private String nickName;

    /**
     * 性别 1：男 2：女
     */
    @ApiModelProperty("性别 1：男 2：女")
    private Integer sex;

    /**
     * 有效状态 1:有效 0:无效
     */
    @ApiModelProperty("有效状态 1:有效 0:无效")
    private Boolean validStatus;

    /**
     * 角色列表
     */
    @ApiModelProperty("角色列表")
    private List<RoleEntity> roles;

    /**
     * 最后登录城市
     */
    @ApiModelProperty("最后登录城市")
    private String lastLoginCity;

    /**
     * 最后登录时间
     */
    @ApiModelProperty("最后登录时间")
    private Date lastLoginTime;
}
