package cn.net.susan.entity.mall.web;

import lombok.Data;

/**
 * 商品组属性值web实体
 *
 * @author 苏三
 * @date 2024/9/7 下午3:24
 */
@Data
public class ProductGroupAttributeValueWebEntity {

    /**
     * 系统ID
     */
    private Long id;

    /**
     * 属性值
     */
    private String value;

}
