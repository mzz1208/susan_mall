package cn.net.susan.mapper.mall;

import cn.net.susan.entity.mall.ProductConditionEntity;
import cn.net.susan.entity.mall.ProductEntity;

import java.util.List;

import cn.net.susan.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品 mapper
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-05-09 14:43:56
 */
public interface ProductMapper extends BaseMapper<ProductEntity, ProductConditionEntity> {
    /**
     * 查询商品信息
     *
     * @param id 商品ID
     * @return 商品信息
     */
    ProductEntity findById(Long id);

    /**
     * 添加商品
     *
     * @param productEntity 商品信息
     * @return 结果
     */
    int insert(ProductEntity productEntity);

    /**
     * 批量添加商品
     *
     * @param productEntities 商品信息
     * @return 结果
     */
    int batchInsert(List<ProductEntity> productEntities);

    /**
     * 修改商品
     *
     * @param productEntity 商品信息
     * @return 结果
     */
    int update(ProductEntity productEntity);

    /**
     * 批量删除商品
     *
     * @param ids    id集合
     * @param entity 商品实体
     * @return 结果
     */
    int deleteByIds(@Param("ids") List<Long> ids, @Param("entity") ProductEntity entity);

    /**
     * 批量查询商品信息
     *
     * @param ids ID集合
     * @return 部门信息
     */
    List<ProductEntity> findByIds(List<Long> ids);
}
