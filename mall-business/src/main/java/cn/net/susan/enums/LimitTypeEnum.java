package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 限流类型
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/4/28 上午11:02
 */
@Getter
@AllArgsConstructor
public enum LimitTypeEnum {

    /**
     * 针对访问接口的所有请求
     */
    ALL(0, "所有"),

    /**
     * 针对访问接口的指定IP
     */
    IP(1, "用户ip"),

    /**
     * 针对访问接口的指定用户
     */
    USER_ID(2, "用户ID");


    /**
     * 枚举值
     */
    private Integer value;


    /**
     * 枚举描述
     */
    private String desc;
}
