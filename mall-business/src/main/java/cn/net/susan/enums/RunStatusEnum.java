package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 执行状态枚举
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/4/30 下午3:39
 */
@AllArgsConstructor
@Getter
public enum RunStatusEnum {

    /**
     * 执行中
     */
    RUNNING(1, "执行中"),

    /**
     * 暂停
     */
    PAUSE(2, "暂停"),

    /**
     * 成功
     */
    SUCCESS(3, "成功"),

    /**
     * 失败
     */
    FAILURE(4, "失败");

    private Integer value;

    private String desc;
}
