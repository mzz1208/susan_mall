package cn.net.susan.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 任务状态枚举
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/29 下午4:46
 */
@Getter
@AllArgsConstructor
public enum TaskStatusEnum {

    WAITING(0, "待执行"),
    RUNNING(1, "执行中"),
    SUCCESS(2, "成功"),
    FAIL(3, "失败");

    /**
     * 枚举值
     */
    private Integer value;


    /**
     * 枚举描述
     */
    private String desc;
}
