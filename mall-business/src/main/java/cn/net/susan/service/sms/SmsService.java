package cn.net.susan.service.sms;

import cn.net.susan.enums.SmsTypeEnum;
import cn.net.susan.util.RandomUtil;
import cn.net.susan.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 短信 service
 *
 * @author 苏三
 * @date 2024/8/8 上午11:52
 */
@Service
public class SmsService {

    private static final String SMS_CODE_PREFIX = "smsCode:";

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ISmsService iSmsService;
    @Value("${mall.mgt.registerSmsCodeExpireSecond:60}")
    private long registerSmsCodeExpireSecond;

    /**
     * 发送注册短信
     *
     * @param phone 手机号
     * @return 是否成功
     */
    public boolean sendRegisterSms(String phone) {
        String smsCodePrefixKey = getSmsCodePrefixKey(phone);
        String value = redisUtil.get(smsCodePrefixKey);
        if (StringUtils.hasLength(value)) {
            return true;
        }

        String sixBitRandom = RandomUtil.getSixBitRandom();
        boolean success = iSmsService.sendCode(phone, sixBitRandom, SmsTypeEnum.REGISTER);
        if (success) {
            redisUtil.set(smsCodePrefixKey, sixBitRandom, registerSmsCodeExpireSecond);
            return true;
        }
        return false;
    }


    private String getSmsCodePrefixKey(String phone) {
        return String.format("%s%s", SMS_CODE_PREFIX, phone);
    }
}
