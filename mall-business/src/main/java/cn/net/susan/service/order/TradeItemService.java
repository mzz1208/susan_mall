package cn.net.susan.service.order;

import java.util.List;

import cn.net.susan.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.net.susan.mapper.order.TradeItemMapper;
import cn.net.susan.entity.order.TradeItemConditionEntity;
import cn.net.susan.entity.order.TradeItemEntity;
import cn.net.susan.entity.ResponsePageEntity;
import cn.net.susan.util.AssertUtil;
import cn.net.susan.util.FillUserUtil;
import cn.net.susan.mapper.BaseMapper;
/**
 * 订单明细 服务层
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-05-30 15:33:05
 */
@Service
public class TradeItemService extends BaseService< TradeItemEntity,  TradeItemConditionEntity> {

	@Autowired
	private TradeItemMapper tradeItemMapper;

	/**
     * 查询订单明细信息
     *
     * @param id 订单明细ID
     * @return 订单明细信息
     */
	public TradeItemEntity findById(Long id) {
	    return tradeItemMapper.findById(id);
	}

	/**
     * 根据条件分页查询订单明细列表
     *
     * @param tradeItemConditionEntity 订单明细信息
     * @return 订单明细集合
     */
	public ResponsePageEntity<TradeItemEntity> searchByPage(TradeItemConditionEntity tradeItemConditionEntity) {
		return super.searchByPage(tradeItemConditionEntity);
	}

    /**
     * 新增订单明细
     *
     * @param tradeItemEntity 订单明细信息
     * @return 结果
     */
	public int insert(TradeItemEntity tradeItemEntity) {
	    return tradeItemMapper.insert(tradeItemEntity);
	}

	/**
     * 修改订单明细
     *
     * @param tradeItemEntity 订单明细信息
     * @return 结果
     */
	public int update(TradeItemEntity tradeItemEntity) {
	    return tradeItemMapper.update(tradeItemEntity);
	}

	/**
     * 批量删除订单明细对象
     *
     * @param ids 系统ID集合
     * @return 结果
     */
	public int deleteByIds(List<Long> ids) {
		List<TradeItemEntity> entities = tradeItemMapper.findByIds(ids);
		AssertUtil.notEmpty(entities, "订单明细已被删除");

		TradeItemEntity entity = new TradeItemEntity();
		FillUserUtil.fillUpdateUserInfo(entity);
		return tradeItemMapper.deleteByIds(ids, entity);
	}

	@Override
	protected BaseMapper getBaseMapper() {
		return tradeItemMapper;
	}

}
