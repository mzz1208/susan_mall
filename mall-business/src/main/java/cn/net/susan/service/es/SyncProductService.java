package cn.net.susan.service.es;

import cn.net.susan.config.BusinessConfig;
import cn.net.susan.constant.NumberConstant;
import cn.net.susan.entity.EsBaseEntity;
import cn.net.susan.entity.ResponsePageEntity;
import cn.net.susan.entity.mall.ProductConditionEntity;
import cn.net.susan.entity.mall.ProductEntity;
import cn.net.susan.entity.mall.ProductPhotoConditionEntity;
import cn.net.susan.entity.mall.ProductPhotoEntity;
import cn.net.susan.entity.mall.web.ProductWebEntity;
import cn.net.susan.es.EsTemplate;
import cn.net.susan.mapper.mall.ProductPhotoMapper;
import cn.net.susan.service.mall.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static cn.net.susan.util.CoverUtil.getCover;

/**
 * 同步ES商品service
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/14 下午4:45
 */
@Slf4j
@Service
public class SyncProductService {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductPhotoMapper productPhotoMapper;
    @Autowired
    private EsTemplate esTemplate;

    @Autowired
    private BusinessConfig businessConfig;

    /**
     * 同步商品到ES
     */
    public void syncProductToES() {
        handleInsertOrUpdate();
        handleDelete();
    }

    private void handleInsertOrUpdate() {
        ProductConditionEntity productConditionEntity = new ProductConditionEntity();
        productConditionEntity.setPageSize(NumberConstant.NUMBER_500);
        productConditionEntity.setIsDel(0);
        ResponsePageEntity<ProductEntity> productEntityResponsePageEntity = productService.searchByPage(productConditionEntity);

        while (CollectionUtils.isNotEmpty(productEntityResponsePageEntity.getData())) {
            saveData(productEntityResponsePageEntity.getData());

            productConditionEntity.setPageNo(productConditionEntity.getPageNo() + 1);
            productEntityResponsePageEntity = productService.searchByPage(productConditionEntity);
        }
    }

    private void handleDelete() {
        ProductConditionEntity productConditionEntity = new ProductConditionEntity();
        productConditionEntity.setPageSize(NumberConstant.NUMBER_500);
        productConditionEntity.setIsDel(1);
        ResponsePageEntity<ProductEntity> productEntityResponsePageEntity = productService.searchByPage(productConditionEntity);

        while (CollectionUtils.isNotEmpty(productEntityResponsePageEntity.getData())) {
            List<Long> idList = productEntityResponsePageEntity.getData().stream().map(ProductEntity::getId).collect(Collectors.toList());
            try {
                esTemplate.deleteBatch(businessConfig.getProductEsIndexName(), idList);
            } catch (IOException e) {
                log.error("删除ES中的商品失败，原因：", e);
            }

            productConditionEntity.setPageNo(productConditionEntity.getPageNo() + 1);
            productEntityResponsePageEntity = productService.searchByPage(productConditionEntity);
        }
    }


    private void saveData(List<ProductEntity> productEntities) {
        ProductPhotoConditionEntity productPhotoConditionEntity = new ProductPhotoConditionEntity();
        productPhotoConditionEntity.setProductIdList(productEntities.stream().map(ProductEntity::getId).collect(Collectors.toList()));
        List<ProductPhotoEntity> productPhotoEntities = productPhotoMapper.searchByCondition(productPhotoConditionEntity);

        List<ProductWebEntity> dataList = productEntities.stream().map(x -> {
            ProductWebEntity productWebEntity = new ProductWebEntity();
            productWebEntity.setId(x.getId().toString());
            productWebEntity.setName(x.getName());
            productWebEntity.setModel(x.getModel());
            productWebEntity.setPrice(x.getPrice());
            productWebEntity.setQuantity(x.getQuantity());
            productWebEntity.setCover(getCover(x.getId(), productPhotoEntities));
            return productWebEntity;
        }).collect(Collectors.toList());

        for (EsBaseEntity esBaseEntity : dataList) {
            esTemplate.insertOrUpdate(businessConfig.getProductEsIndexName(), esBaseEntity);
        }
        //esTemplate.batchInsert(businessConfig.getProductEsIndexName(), dataList);
    }
}
