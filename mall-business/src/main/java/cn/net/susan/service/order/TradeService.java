package cn.net.susan.service.order;

import cn.net.susan.entity.ResponsePageEntity;
import cn.net.susan.entity.auth.JwtUserEntity;
import cn.net.susan.entity.mall.ProductConditionEntity;
import cn.net.susan.entity.mall.ProductEntity;
import cn.net.susan.entity.order.TradeConditionEntity;
import cn.net.susan.entity.order.TradeEntity;
import cn.net.susan.entity.order.TradeItemEntity;
import cn.net.susan.entity.seckill.UserSeckillProductTradeEntity;
import cn.net.susan.entity.sys.UserConditionEntity;
import cn.net.susan.entity.sys.UserEntity;
import cn.net.susan.enums.OrderStatusEnum;
import cn.net.susan.enums.OrderTypeEnum;
import cn.net.susan.exception.BusinessException;
import cn.net.susan.helper.IdGenerateHelper;
import cn.net.susan.helper.IpWhiteListHelper;
import cn.net.susan.helper.MqHelper;
import cn.net.susan.mapper.BaseMapper;
import cn.net.susan.mapper.mall.ProductMapper;
import cn.net.susan.mapper.order.TradeMapper;
import cn.net.susan.mapper.sys.UserMapper;
import cn.net.susan.service.BaseService;
import cn.net.susan.util.AssertUtil;
import cn.net.susan.util.FillUserUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.google.common.collect.Lists;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static cn.net.susan.config.RabbitConfig.TRADE_STATUS_CHANGE_ROUTING_KEY_PREFIX;
import static cn.net.susan.util.OrderCodeUtil.ORDER_CODE_LENGTH;

/**
 * 订单 服务层
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-05-30 15:33:03
 */
@Service
public class TradeService extends BaseService<TradeEntity, TradeConditionEntity> {

    @Autowired
    private TradeMapper tradeMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private IdGenerateHelper idGenerateHelper;
    @Autowired
    private TradeSaveService tradeSaveService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MqHelper mqHelper;
    @Autowired
    private IpWhiteListHelper ipWhiteListHelper;

    @Value("${mall.mgt.httpRequest.secretKey:susan123}")
    private String secretKey;

    @Value("${mall.mgt.tradeStatusChangeTopic:TRADE_STATUS_CHANGE_TOPIC}")
    private String tradeStatusChangeTopic;

    @Value("${mall.mgt.overTimeCancelTradeTopic:OVER_TIME_CANCEL_TRADE_TOPIC}")
    private String overTimeCancelTradeTopic;

    /**
     * 处理订单过期取消消息
     *
     * @param tradeEntity 订单实体
     */
    public void handleOverTimeCancelTrade(TradeEntity tradeEntity) {
        //获取代理对象
        TradeService tradeProxyService = (TradeService) AopContext.currentProxy();
        TradeEntity tradeEntityFromDB = tradeProxyService.findById(tradeEntity.getId());
        AssertUtil.notNull(tradeEntityFromDB, "订单不存在");

        //如果订单过期了，但还是下单状态，则需要自动取消订单
        if (OrderStatusEnum.CREATE.getValue().equals(tradeEntityFromDB.getOrderStatus())) {
            TradeEntity updateEntity = new TradeEntity();
            updateEntity.setOrderStatus(OrderStatusEnum.CANCEL.getValue());
            updateEntity.setUpdateTime(new Date());
            updateEntity.setId(tradeEntityFromDB.getId());
            tradeProxyService.update(updateEntity);

            if (OrderTypeEnum.SECKILL_PRODUCT.getValue().equals(tradeEntityFromDB.getOrderType())) {
                //通知下游业务系统订单状态已变化
                mqHelper.send(overTimeCancelTradeTopic, tradeEntity);
            }
        }
    }

    private String getTradeStatusChangeKey(Long id) {
        return String.format("%s%s", TRADE_STATUS_CHANGE_ROUTING_KEY_PREFIX, id);
    }


    /**
     * 处理用户秒杀商品订单
     *
     * @param userSeckillProductTradeEntity 用户秒杀商品订单实体
     * @return 用户秒杀商品订单
     */
    public UserSeckillProductTradeEntity createUserSeckillProductTrade(UserSeckillProductTradeEntity userSeckillProductTradeEntity) {
        try {
            AssertUtil.notNull(userSeckillProductTradeEntity.getProductId(), "productId不能为空");
            AssertUtil.isTrue(StringUtils.hasLength(userSeckillProductTradeEntity.getUserName()), "userName不能为空");
            FillUserUtil.setCurrentUser(null, userSeckillProductTradeEntity.getUserName());

            UserConditionEntity userConditionEntity = new UserConditionEntity();
            userConditionEntity.setUserName(userSeckillProductTradeEntity.getUserName());
            List<UserEntity> userEntities = userMapper.searchByCondition(userConditionEntity);
            AssertUtil.notEmpty(userEntities, String.format("该用户[%s]不存在", userSeckillProductTradeEntity.getUserName()));
            UserEntity userEntity = userEntities.get(0);

            ProductEntity productEntity = productMapper.findById(userSeckillProductTradeEntity.getProductId());
            AssertUtil.notNull(productEntity, String.format("订单[%s]不存在", userSeckillProductTradeEntity.getProductId()));

            TradeEntity tradeEntity = createTradeEntity(userSeckillProductTradeEntity, productEntity, userEntity);
            TradeEntity oldTradeEntity = tradeSaveService.checkRepeat(tradeEntity, false);
            if (Objects.nonNull(oldTradeEntity)) {
                fillData(userSeckillProductTradeEntity, userEntity, productEntity, oldTradeEntity);
                tradeEntity.setId(oldTradeEntity.getId());
                return userSeckillProductTradeEntity;
            }

            tradeEntity.setOrderType(OrderTypeEnum.SECKILL_PRODUCT.getValue());
            FillUserUtil.setCurrentUser(userEntity.getId(), userEntity.getUserName());
            tradeSaveService.createTrade(FillUserUtil.getCurrentUserInfo(), tradeEntity);
            fillData(userSeckillProductTradeEntity, userEntity, productEntity, tradeEntity);
        } finally {
            FillUserUtil.clearCurrentUser();
        }

        return userSeckillProductTradeEntity;
    }

    private void fillData(UserSeckillProductTradeEntity userSeckillProductTradeEntity,
                          UserEntity userEntity,
                          ProductEntity productEntity,
                          TradeEntity tradeEntity) {
        userSeckillProductTradeEntity.setCostPrice(productEntity.getPrice());
        userSeckillProductTradeEntity.setProductName(productEntity.getName());
        userSeckillProductTradeEntity.setModel(productEntity.getModel());
        userSeckillProductTradeEntity.setOrderTime(tradeEntity.getOrderTime());
        userSeckillProductTradeEntity.setTradeId(tradeEntity.getId());
        userSeckillProductTradeEntity.setUserId(userEntity.getId());
    }

    private TradeEntity createTradeEntity(UserSeckillProductTradeEntity userSeckillProductTradeEntity, ProductEntity productEntity, UserEntity userEntity) {
        TradeEntity tradeEntity = new TradeEntity();
        tradeEntity.setCode(userSeckillProductTradeEntity.getCode());
        tradeEntity.setTotalAmount(userSeckillProductTradeEntity.getTotalAmount());
        tradeEntity.setPaymentAmount(userSeckillProductTradeEntity.getPaymentAmount());
        tradeEntity.setUserName(userSeckillProductTradeEntity.getUserName());
        tradeEntity.setUserId(userEntity.getId());

        TradeItemEntity tradeItemEntity = new TradeItemEntity();
        tradeItemEntity.setCode(userSeckillProductTradeEntity.getCode());
        tradeItemEntity.setProductId(userSeckillProductTradeEntity.getProductId());
        tradeItemEntity.setModel(productEntity.getModel());
        tradeItemEntity.setProductName(productEntity.getName());
        tradeItemEntity.setPrice(userSeckillProductTradeEntity.getPrice());
        tradeItemEntity.setAmount(userSeckillProductTradeEntity.getTotalAmount());
        tradeItemEntity.setQuantity(userSeckillProductTradeEntity.getQuantity());
        tradeEntity.setTradeItemEntityList(Lists.newArrayList(tradeItemEntity));
        return tradeEntity;
    }

    /**
     * 查询订单信息
     *
     * @param id 订单ID
     * @return 订单信息
     */
    @DS("sharding")
    public TradeEntity findById(Long id) {
        return tradeMapper.findById(id);
    }

    /**
     * 根据条件分页查询订单列表
     *
     * @param tradeConditionEntity 订单信息
     * @return 订单集合
     */
    @DS("sharding")
    public ResponsePageEntity<TradeEntity> searchByPage(TradeConditionEntity tradeConditionEntity) {
        return super.searchByPage(tradeConditionEntity);
    }

    /**
     * 用户下单
     *
     * @param tradeEntity 订单信息
     * @return 结果
     */

    public void create(TradeEntity tradeEntity) {
        JwtUserEntity currentUserInfo = FillUserUtil.getCurrentUserInfo();
        checkParam(tradeEntity);
        tradeSaveService.checkRepeat(tradeEntity, true);

        tradeEntity.setOrderType(OrderTypeEnum.NORMAL_PRODUCT.getValue());
        fillTradeItemEntity(tradeEntity);

        tradeSaveService.createTrade(currentUserInfo, tradeEntity);
    }

    private void fillTradeItemEntity(TradeEntity tradeEntity) {
        List<Long> productIdList = tradeEntity.getTradeItemEntityList()
                .stream().map(TradeItemEntity::getProductId).collect(Collectors.toList());

        ProductConditionEntity productConditionEntity = new ProductConditionEntity();
        productConditionEntity.setIdList(productIdList);
        productConditionEntity.setPageSize(0);
        List<ProductEntity> productEntities = productMapper.searchByCondition(productConditionEntity);
        AssertUtil.notEmpty(productEntities, "商品不能为空");

        List<Long> notFoundList = productIdList.stream().filter(x -> productEntities.stream().noneMatch(c -> x.equals(c.getId()))).collect(Collectors.toList());
        AssertUtil.isTrue(org.apache.commons.collections4.CollectionUtils.isEmpty(notFoundList), String.format("商品ID：%s，在系统中不存在", notFoundList));

        for (TradeItemEntity tradeItemEntity : tradeEntity.getTradeItemEntityList()) {
            ProductEntity productEntity = productEntities.stream().filter(x -> x.getId().equals(tradeItemEntity.getProductId()))
                    .findAny().orElseThrow(() -> new BusinessException(String.format("商品ID：%s，在系统中不存在", tradeItemEntity.getProductId())));
            tradeItemEntity.setProductName(productEntity.getName());
            tradeItemEntity.setModel(productEntity.getModel());
            tradeItemEntity.setId(idGenerateHelper.nextId());
        }
    }

    private void checkParam(TradeEntity tradeEntity) {
        AssertUtil.isTrue(StringUtils.hasLength(tradeEntity.getCode()), "订单编码不能为空");
        AssertUtil.isTrue(tradeEntity.getCode().length() == ORDER_CODE_LENGTH, String.format("订单编码要求必须是%s位的", ORDER_CODE_LENGTH));

        AssertUtil.isTrue(checkGtZero(tradeEntity.getTotalAmount()), "总金额必须大于0");
        AssertUtil.isTrue(checkLtZero(tradeEntity.getTotalAmount(), 100000), "总金额必须小于100000");
        AssertUtil.isTrue(checkGtZero(tradeEntity.getPaymentAmount()), "付款金额必须大于0");
        AssertUtil.isTrue(checkLtZero(tradeEntity.getPaymentAmount(), 100000), "总金额必须小于100000");

        for (TradeItemEntity tradeItemEntity : tradeEntity.getTradeItemEntityList()) {
            AssertUtil.isTrue(checkGtZero(tradeItemEntity.getAmount()), "金额必须大于0");
            AssertUtil.isTrue(checkLtZero(tradeItemEntity.getAmount(), 100000), "金额必须小于10000");
            AssertUtil.isTrue(checkGtZero(tradeItemEntity.getPrice()), "单价必须大于0");
            AssertUtil.isTrue(checkLtZero(tradeItemEntity.getPrice(), 100000), "单价必须小于10000");
            AssertUtil.isTrue(tradeItemEntity.getQuantity() > 0, "数量必须大于0");
        }
    }

    private boolean checkGtZero(BigDecimal value) {
        return value.compareTo(BigDecimal.ZERO) > 0;
    }

    private boolean checkLtZero(BigDecimal value, Integer maxValue) {
        return value.compareTo(new BigDecimal(maxValue)) < 0;
    }


    /**
     * 修改订单
     *
     * @param tradeEntity 订单信息
     * @return 结果
     */
    @DS("sharding")
    public int update(TradeEntity tradeEntity) {
        return tradeMapper.update(tradeEntity);
    }

    /**
     * 批量删除订单对象
     *
     * @param ids 系统ID集合
     * @return 结果
     */
    @DS("sharding")
    public int deleteByIds(List<Long> ids) {
        List<TradeEntity> entities = tradeMapper.findByIds(ids);
        AssertUtil.notEmpty(entities, "订单已被删除");

        TradeEntity entity = new TradeEntity();
        FillUserUtil.fillUpdateUserInfo(entity);
        return tradeMapper.deleteByIds(ids, entity);
    }

    @Override
    protected BaseMapper getBaseMapper() {
        return tradeMapper;
    }

}
