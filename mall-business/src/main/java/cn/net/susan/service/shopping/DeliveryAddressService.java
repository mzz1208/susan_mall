package cn.net.susan.service.shopping;

import java.util.List;

import cn.net.susan.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.net.susan.mapper.shopping.DeliveryAddressMapper;
import cn.net.susan.entity.shopping.DeliveryAddressConditionEntity;
import cn.net.susan.entity.shopping.DeliveryAddressEntity;
import cn.net.susan.entity.ResponsePageEntity;
import cn.net.susan.util.AssertUtil;
import cn.net.susan.util.FillUserUtil;
import cn.net.susan.mapper.BaseMapper;

 /**
 * 收货地址 服务层
 *
 * @author 苏三 该项目是知识星球：java突击队 的内部项目
 * @date 2024-09-01 10:02:01
 */
@Service
public class DeliveryAddressService extends BaseService<DeliveryAddressEntity, DeliveryAddressConditionEntity> {

	@Autowired
	private DeliveryAddressMapper deliveryAddressMapper;

	/**
     * 查询收货地址信息
     *
     * @param id 收货地址ID
     * @return 收货地址信息
     */
	public DeliveryAddressEntity findById(Long id) {
	    return deliveryAddressMapper.findById(id);
	}

	/**
     * 根据条件分页查询收货地址列表
     *
     * @param deliveryAddressConditionEntity 收货地址信息
     * @return 收货地址集合
     */
	public ResponsePageEntity<DeliveryAddressEntity> searchByPage(DeliveryAddressConditionEntity deliveryAddressConditionEntity) {
		return super.searchByPage(deliveryAddressConditionEntity);
	}

    /**
     * 新增收货地址
     *
     * @param deliveryAddressEntity 收货地址信息
     * @return 结果
     */
	public int insert(DeliveryAddressEntity deliveryAddressEntity) {
	    return deliveryAddressMapper.insert(deliveryAddressEntity);
	}

	/**
     * 修改收货地址
     *
     * @param deliveryAddressEntity 收货地址信息
     * @return 结果
     */
	public int update(DeliveryAddressEntity deliveryAddressEntity) {
	    return deliveryAddressMapper.update(deliveryAddressEntity);
	}

	/**
     * 批量删除收货地址
     *
     * @param ids 系统ID集合
     * @return 结果
     */
	public int deleteByIds(List<Long> ids) {
		List<DeliveryAddressEntity> entities = deliveryAddressMapper.findByIds(ids);
		AssertUtil.notEmpty(entities, "收货地址已被删除");

		DeliveryAddressEntity entity = new DeliveryAddressEntity();
		FillUserUtil.fillUpdateUserInfo(entity);
		return deliveryAddressMapper.deleteByIds(ids, entity);
	}

    @Override
    protected BaseMapper getBaseMapper() {
        return deliveryAddressMapper;
    }
}
