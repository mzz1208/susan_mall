-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: susan_mall
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `biz_log`
--

DROP TABLE IF EXISTS `biz_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `biz_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `method_name` varchar(50) NOT NULL COMMENT '方法名称',
  `description` varchar(30) NOT NULL COMMENT '描述',
  `request_ip` varchar(15) NOT NULL COMMENT '请求ip',
  `browser` varchar(200) DEFAULT NULL COMMENT '浏览器',
  `url` varchar(100) NOT NULL COMMENT '请求地址',
  `param` varchar(300) DEFAULT NULL COMMENT '请求参数',
  `time` int NOT NULL COMMENT '耗时',
  `exception` varchar(300) DEFAULT NULL COMMENT '异常',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1:成功 0:失败',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除  `is_del` tinyint(1) D除',
  `city` varchar(10) DEFAULT NULL COMMENT '所在城市',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='业务日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_log`
--

LOCK TABLES `biz_log` WRITE;
/*!40000 ALTER TABLE `biz_log` DISABLE KEYS */;
INSERT INTO `biz_log` VALUES (1,'cn.net.susan.controller.sys.DeptController.insert','添加部门','127.0.0.1','Chrome','/v1/dept/insert',NULL,615077687,NULL,1,13,'admin','2024-02-24 21:38:28.061',NULL,NULL,NULL,0,''),(2,'cn.net.susan.controller.sys.DeptController.insert','添加部门','127.0.0.1','Chrome','/v1/dept/insert',NULL,614935214,NULL,1,13,'admin','2024-02-24 21:40:52.034',NULL,NULL,NULL,0,''),(3,'cn.net.susan.controller.sys.DeptController.insert','添加部门','127.0.0.1','Chrome','/v1/dept/insert','{ deptEntity: DeptEntity(name=测试, pid=0, validStatus=null, roleId=null)}',612718087,NULL,1,13,'admin','2024-02-24 22:17:48.825',NULL,NULL,NULL,0,''),(4,'cn.net.susan.controller.sys.DeptController.insert','添加部门','127.0.0.1','Chrome','/v1/dept/insert','{}',612237467,NULL,1,13,'admin','2024-02-24 22:26:57.138',NULL,NULL,NULL,0,''),(5,'cn.net.susan.controller.sys.DeptController.insert','添加部门','127.0.0.1','Chrome','/v1/dept/insert','{}',612157347,NULL,1,13,'admin','2024-02-24 22:27:22.160',NULL,NULL,NULL,0,''),(6,'cn.net.susan.controller.sys.DeptController.insert','添加部门','127.0.0.1','Chrome','/v1/dept/insert','{ deptEntity: DeptEntity(name=市场部, pid=0, validStatus=null, roleId=null)}',1996452299,NULL,1,13,'admin','2024-03-29 14:58:19.951',NULL,NULL,NULL,0,'内网IP');
/*!40000 ALTER TABLE `biz_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_notify`
--

DROP TABLE IF EXISTS `common_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `common_notify` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint NOT NULL COMMENT '通知类型 1：excel导出',
  `title` varchar(30) NOT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `link_url` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  `read_status` tinyint NOT NULL DEFAULT '0' COMMENT '阅读状态 0：未阅读 1：已阅读',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  `to_user_id` bigint NOT NULL COMMENT '需要推送通知的用户ID',
  `is_push` tinyint(1) DEFAULT '0' COMMENT '是否已推送 1：已推送 0：未推送',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='通知表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_notify`
--

LOCK TABLES `common_notify` WRITE;
/*!40000 ALTER TABLE `common_notify` DISABLE KEYS */;
INSERT INTO `common_notify` VALUES (1,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:43:31.802',1,'系统管理员','2024-02-06 15:43:31.916',0,1,1),(2,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:43:31.892',1,'系统管理员','2024-02-06 15:43:31.931',0,1,1),(3,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:44:47.072',1,'系统管理员','2024-02-06 15:44:52.263',0,1,1),(4,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:44:52.247',1,'系统管理员','2024-02-06 15:44:52.268',0,1,1),(5,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:49:55.539',1,'系统管理员','2024-02-06 15:50:05.644',0,1,1),(6,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:49:56.825',1,'系统管理员','2024-02-06 15:50:05.654',0,1,1),(7,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,1,'系统管理员','2024-02-06 15:51:40.563',1,'系统管理员','2024-02-06 15:51:40.595',0,1,1),(8,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,13,'admin','2024-02-06 16:30:37.383',13,'admin','2024-02-06 16:31:06.641',0,13,1),(9,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,13,'admin','2024-02-06 16:30:37.473',13,'admin','2024-02-06 16:31:53.068',0,13,1),(10,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,13,'admin','2024-02-06 16:31:53.472',13,'admin','2024-02-06 16:34:50.309',0,13,1),(11,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,13,'admin','2024-02-06 16:34:51.071',13,'admin','2024-02-06 16:35:27.953',0,13,1),(12,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,13,'admin','2024-02-06 16:38:34.934',13,'admin','2024-02-06 16:38:41.073',0,13,1),(13,1,'excel导出通知','已经帮您导出excel文件：导出菜单数据，下载地址：null',NULL,0,13,'admin','2024-02-06 16:38:35.179',13,'admin','2024-02-06 16:38:43.566',0,13,1),(14,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：<a href=\'/tmp/菜单数据_2024-02-06.xlsx\'>',NULL,0,13,'admin','2024-02-06 16:48:35.759',13,'admin','2024-02-06 16:48:39.350',0,13,1),(15,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：<a href=\'/tmp/菜单数据_2024-02-06.xlsx\'>',NULL,0,13,'admin','2024-02-06 16:49:31.544',13,'admin','2024-02-06 16:49:36.947',0,13,1),(16,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：<a href=\'/tmp/菜单数据_2024-02-06.xlsx\'>',NULL,0,13,'admin','2024-02-06 17:00:11.280',13,'admin','2024-02-06 17:00:18.204',0,13,1),(17,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：<a href=\'/tmp/菜单数据_2024-02-06.xlsx\'>',NULL,0,13,'admin','2024-02-06 17:01:07.420',13,'admin','2024-02-06 17:01:12.723',0,13,1),(18,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-02-06.xlsx',NULL,0,13,'admin','2024-02-06 17:45:34.570',13,'admin','2024-02-06 17:45:43.037',0,13,1),(19,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-02-06.xlsx',NULL,0,13,'admin','2024-02-06 18:06:35.133',13,'admin','2024-02-06 18:06:35.442',0,13,1),(20,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-02-06.xlsx',NULL,0,13,'admin','2024-02-06 18:07:14.922',13,'admin','2024-02-06 18:07:15.579',0,13,1),(21,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 17:07:07.531',13,'admin','2024-03-10 17:07:12.115',0,13,1),(22,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 17:15:47.123',13,'admin','2024-03-10 17:15:54.442',0,13,1),(23,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 17:22:08.827',13,'admin','2024-03-10 17:22:46.629',0,13,1),(24,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 17:25:53.546',13,'admin','2024-03-10 17:25:58.241',0,13,1),(25,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:02:33.667',NULL,NULL,NULL,0,13,0),(26,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:04:39.842',NULL,NULL,NULL,0,13,0),(27,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:08:21.944',NULL,NULL,NULL,0,13,0),(28,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:11:22.782',NULL,NULL,NULL,0,13,0),(29,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:14:09.457',NULL,NULL,NULL,0,13,0),(30,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:15:53.486',NULL,NULL,NULL,0,13,0),(31,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:19:38.004',NULL,NULL,NULL,0,13,0),(32,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:27:17.663',NULL,NULL,NULL,0,13,0),(33,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:28:34.879',NULL,NULL,NULL,0,13,0),(34,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:31:05.781',NULL,NULL,NULL,0,13,0),(35,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:38:32.436',NULL,NULL,NULL,0,13,0),(36,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:41:25.718',NULL,NULL,NULL,0,13,0),(37,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:45:52.027',NULL,NULL,NULL,0,13,0),(38,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-10.xlsx',NULL,0,13,'admin','2024-03-10 18:50:47.350',NULL,NULL,NULL,0,13,0),(39,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 15:50:11.729',NULL,NULL,NULL,0,13,0),(40,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 16:03:11.243',NULL,NULL,NULL,0,13,0),(41,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 16:10:09.295',NULL,NULL,NULL,0,13,0),(42,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 16:15:02.136',NULL,NULL,NULL,0,13,0),(43,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 16:16:50.619',NULL,NULL,NULL,0,13,0),(44,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 16:26:43.257',NULL,NULL,NULL,0,13,0),(45,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-11.xlsx',NULL,0,13,'admin','2024-03-11 16:28:17.937',NULL,NULL,NULL,0,13,0),(46,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-16.xlsx',NULL,0,13,'admin','2024-03-16 10:07:12.362',NULL,NULL,NULL,0,13,0),(47,1,'excel导出通知','成功导出excel文件：导出菜单数据，下载地址：/tmp/菜单数据_2024-03-16.xlsx',NULL,0,13,'admin','2024-03-16 10:10:29.938',NULL,NULL,NULL,0,13,0);
/*!40000 ALTER TABLE `common_notify` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_task`
--

DROP TABLE IF EXISTS `common_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `common_task` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) NOT NULL COMMENT '任务名称',
  `file_url` varchar(255) DEFAULT NULL COMMENT '下载文件地址',
  `type` tinyint NOT NULL COMMENT '任务类型 1：通用excel导出',
  `status` tinyint NOT NULL COMMENT '执行状态 0：待执行 1：执行中 2：成功 3：失败',
  `failure_count` tinyint DEFAULT '0' COMMENT '失败次数',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  `biz_type` tinyint NOT NULL COMMENT '业务类型 1：菜单 2：部门 3：角色 4：用户',
  `request_param` json DEFAULT NULL COMMENT '请求参数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='任务表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_task`
--

LOCK TABLES `common_task` WRITE;
/*!40000 ALTER TABLE `common_task` DISABLE KEYS */;
INSERT INTO `common_task` VALUES (1,'导出菜单数据','/tmp/菜单数据_2024-03-16.xlsx',1,2,0,13,'admin','2024-01-29 18:04:38.229',13,'admin','2024-03-16 10:10:29.935',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(2,'导出菜单数据',NULL,1,2,0,13,'admin','2024-01-30 15:33:12.791',13,'admin','2024-01-30 17:21:29.039',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(3,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-01 22:48:05.232',13,'admin','2024-02-01 22:51:19.106',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(4,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-01 22:54:34.993',13,'admin','2024-02-01 22:56:25.731',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(5,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-01 22:54:53.115',13,'admin','2024-02-01 22:56:56.264',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(6,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-01 22:54:54.367',13,'admin','2024-02-01 22:56:58.146',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(7,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-04 22:00:08.179',13,'admin','2024-02-04 22:17:26.762',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(8,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:16:38.642',1,'系统管理员','2024-02-04 22:17:33.506',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(9,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:18:03.711',1,'系统管理员','2024-02-04 22:19:04.696',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(10,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:19:08.396',1,'系统管理员','2024-02-04 22:21:10.303',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(11,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:21:59.785',1,'系统管理员','2024-02-04 22:27:32.736',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(12,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:21:59.857',1,'系统管理员','2024-02-04 22:27:45.955',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(13,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:22:48.225',1,'系统管理员','2024-02-04 22:27:49.573',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(14,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:24:36.691',1,'系统管理员','2024-02-04 22:27:51.313',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(15,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:26:24.073',1,'系统管理员','2024-02-04 22:27:53.481',0,1,NULL),(16,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:28:18.728',1,'系统管理员','2024-02-04 22:29:02.682',0,1,NULL),(17,'导出菜单数据',NULL,1,3,3,1,'系统管理员','2024-02-04 22:30:26.168',1,'系统管理员','2024-02-04 22:36:45.231',0,1,'{\"pageNo\": 1, \"pageSize\": 15, \"customizeColumnNameList\": [\"name\"]}'),(18,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-04 22:38:54.660',1,'系统管理员','2024-02-04 22:39:42.201',0,1,'{\"pageNo\": 1, \"pageSize\": 15, \"customizeColumnNameList\": [\"name\"]}'),(19,'导出菜单数据',NULL,1,2,2,1,'系统管理员','2024-02-04 22:47:29.439',1,'系统管理员','2024-02-04 22:51:42.080',0,1,'{\"pageNo\": 1, \"pageSize\": 15, \"customizeColumnNameList\": [\"菜单名称\", \"排序\"]}'),(20,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:36:21.472',1,'系统管理员','2024-02-06 15:37:00.954',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(21,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:42:55.545',1,'系统管理员','2024-02-06 15:43:31.800',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(22,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:43:15.842',1,'系统管理员','2024-02-06 15:43:31.885',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(23,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:44:23.818',1,'系统管理员','2024-02-06 15:44:46.002',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(24,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:44:25.259',1,'系统管理员','2024-02-06 15:44:52.243',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(25,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:49:08.437',1,'系统管理员','2024-02-06 15:49:55.536',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(26,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:49:21.714',1,'系统管理员','2024-02-06 15:49:56.821',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(27,'导出菜单数据',NULL,1,2,0,1,'系统管理员','2024-02-06 15:51:26.343',1,'系统管理员','2024-02-06 15:51:40.560',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(28,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-06 16:29:41.521',13,'admin','2024-02-06 16:30:37.380',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(29,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-06 16:30:25.704',13,'admin','2024-02-06 16:30:37.469',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(30,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-06 16:31:44.993',13,'admin','2024-02-06 16:31:53.470',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(31,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-06 16:34:02.042',13,'admin','2024-02-06 16:34:51.011',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(32,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-06 16:38:12.793',13,'admin','2024-02-06 16:38:34.932',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(33,'导出菜单数据',NULL,1,2,0,13,'admin','2024-02-06 16:38:24.019',13,'admin','2024-02-06 16:38:35.176',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(34,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 16:48:24.020',13,'admin','2024-02-06 16:48:35.758',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(35,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 16:48:51.250',13,'admin','2024-02-06 16:49:31.513',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(36,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 16:59:57.056',13,'admin','2024-02-06 17:00:11.269',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(37,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 17:01:05.129',13,'admin','2024-02-06 17:01:07.414',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(38,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 17:45:22.552',13,'admin','2024-02-06 17:45:34.558',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(39,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 18:06:28.316',13,'admin','2024-02-06 18:06:35.129',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(40,'导出菜单数据','/tmp/菜单数据_2024-02-06.xlsx',1,2,0,13,'admin','2024-02-06 18:07:12.261',13,'admin','2024-02-06 18:07:14.906',0,1,'{\"pageNo\": 1, \"pageSize\": 10}'),(41,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 14:53:29.421',13,'admin','2024-03-05 15:08:20.256',0,1,'{\"ip\": \"127.0.0.1\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"内网IP\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-48-05 14:48:51\"}'),(42,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 14:56:10.539',13,'admin','2024-03-05 15:08:21.052',0,1,'{\"ip\": \"127.0.0.1\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-48-05 14:48:51\"}'),(43,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 14:56:43.342',13,'admin','2024-03-05 15:08:21.318',0,1,'{\"ip\": \"127.0.0.1\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-48-05 14:48:51\"}'),(44,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 14:59:07.358',13,'admin','2024-03-05 15:08:21.548',0,1,'{\"ip\": \"127.0.0.1\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"杭州\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-48-05 14:48:51\"}'),(45,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 14:59:29.648',13,'admin','2024-03-05 15:08:21.942',0,1,'{\"ip\": \"127.0.0.1\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"杭州\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:07\"}'),(46,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 15:00:58.592',13,'admin','2024-03-05 15:08:22.152',0,1,'{\"ip\": \"127.0.0.1\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(47,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 15:03:24.108',13,'admin','2024-03-05 15:08:27.469',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(48,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 15:12:27.737',13,'admin','2024-03-05 15:12:39.307',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(49,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 15:16:38.551',13,'admin','2024-03-05 15:17:21.321',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(50,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 15:23:28.761',13,'admin','2024-03-05 15:26:49.089',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(51,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 15:26:13.941',13,'admin','2024-03-05 15:29:29.104',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(52,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 15:30:40.627',13,'admin','2024-03-05 15:30:49.124',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(53,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 15:58:49.516',13,'admin','2024-03-05 15:59:50.349',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(54,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 16:10:07.478',13,'admin','2024-03-05 16:10:49.205',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(55,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 16:12:21.463',13,'admin','2024-03-05 16:12:31.950',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(56,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 16:17:03.445',13,'admin','2024-03-05 16:17:22.017',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(57,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-05 16:19:13.728',13,'admin','2024-03-05 16:19:25.673',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(58,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 17:06:25.198',13,'admin','2024-03-06 16:55:24.151',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(59,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-05 17:06:41.428',13,'admin','2024-03-06 16:55:27.575',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(60,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-06 16:52:30.777',13,'admin','2024-03-06 16:55:30.995',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(61,'发送异地登录邮件',NULL,2,2,0,13,'admin','2024-03-06 16:59:02.813',13,'admin','2024-03-06 16:59:21.490',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(62,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-07 11:59:44.365',13,'admin','2024-03-10 17:05:32.764',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(63,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-07 12:01:07.750',13,'admin','2024-03-10 17:05:33.012',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-05 14:59:29\"}'),(64,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-07 16:38:34.972',13,'admin','2024-03-10 17:05:33.279',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-01-07 12:01:07\"}'),(65,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-07 16:49:10.662',13,'admin','2024-03-10 17:05:33.492',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-38-07 16:38:35\"}'),(66,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-08 16:09:32.849',13,'admin','2024-03-10 17:05:33.676',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36 Aoyou/CnJTZyk1IWdjMCVWZ2orYVdxi57K-CDIAXvsDccQTfQCccPHQCfuoQz0lQ==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-49-07 16:49:10\"}'),(67,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-08 16:35:02.735',13,'admin','2024-03-10 17:05:33.834',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36 Aoyou/DSopKF9PJztLb0IyM2dePUBeQh1MtFa7Fmu42Ox4uCrc2Nq8RLSj7-njyg==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-09-08 16:09:33\"}'),(68,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-08 16:41:18.553',13,'admin','2024-03-10 17:05:34.031',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-35-08 16:35:03\"}'),(69,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-08 16:42:10.904',13,'admin','2024-03-10 17:05:34.202',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-35-08 16:35:03\"}'),(70,'发送异地登录邮件',NULL,2,3,3,13,'admin','2024-03-15 20:57:47.693',13,'admin','2024-03-16 09:54:26.046',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/eyUpCnwqJVRlbHJwMWBdV5zPJqjDlIZGaroMuBrb9R_Uni3llv_D1tYP6w==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-57-15 20:57:19\"}'),(71,'发送异地登录邮件',NULL,2,1,1,13,'admin','2024-03-18 17:08:36.842',13,'admin','2024-03-28 13:19:53.635',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/L29VUzlRaSgwCUpALE9QQYO26lo2iNqVnJp1GCXW0_1uhjoEH8QMGrUONA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-02-18 16:02:00\"}'),(72,'发送异地登录邮件',NULL,2,1,1,13,'admin','2024-03-18 17:27:09.262',13,'admin','2024-03-28 13:20:24.173',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/Li15Tj0MDSYvRDF6aSUqZXg-d9Ip2CCZMC_Ex9JXU1DUuLV60VAATp-Jlg==\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-08-18 17:08:37\"}'),(73,'发送异地登录邮件',NULL,2,1,1,13,'admin','2024-03-18 17:36:47.751',13,'admin','2024-03-28 13:20:55.956',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/Li15Tj0MDSYvRDF6aSUqZXg-d9Ip2CCZMC_Ex9JXU1DUuLV60VAATp-Jlg==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-27-18 17:27:09\"}'),(74,'发送异地登录邮件',NULL,2,1,1,13,'admin','2024-03-18 17:43:58.942',13,'admin','2024-03-28 13:21:27.552',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/Li15Tj0MDSYvRDF6aSUqZXg-d9Ip2CCZMC_Ex9JXU1DUuLV60VAATp-Jlg==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-36-18 17:36:47\"}'),(75,'发送异地登录邮件',NULL,2,1,1,13,'admin','2024-03-18 17:57:07.197',13,'admin','2024-03-28 13:21:58.396',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-43-18 17:43:59\"}'),(76,'发送异地登录邮件',NULL,2,1,1,13,'admin','2024-03-18 19:43:47.642',13,'admin','2024-03-28 13:22:29.363',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-57-18 17:57:07\"}'),(77,'发送异地登录邮件',NULL,2,1,0,13,'admin','2024-03-18 19:55:26.035',13,'admin','2024-03-28 13:22:29.371',0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-43-18 19:43:47\"}'),(78,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-18 20:13:01.967',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-55-18 19:55:26\"}'),(79,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-18 20:30:20.192',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-13-18 20:13:02\"}'),(80,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-18 20:43:25.681',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-30-18 20:30:20\"}'),(81,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-19 09:32:17.262',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-43-18 20:43:25\"}'),(82,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-19 09:39:16.132',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-32-19 09:32:17\"}'),(83,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-19 09:46:40.604',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-39-19 09:39:16\"}'),(84,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-19 12:04:45.581',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-46-19 09:46:40\"}'),(85,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-21 18:20:25.080',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/bmksWT8KI3o6ZkVudgx2Xw1pWjWz8QNOltzbdJwrG0r9xN2gTstrMmROQA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-53-21 17:53:02\"}'),(86,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-21 19:22:55.497',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/bmksWT8KI3o6ZkVudgx2Xw1pWjWz8QNOltzbdJwrG0r9xN2gTstrMmROQA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-20-21 18:20:25\"}'),(87,'导出岗位数据',NULL,1,0,0,13,'admin','2024-03-21 19:38:47.500',NULL,NULL,NULL,0,5,'{\"pageNo\": 1, \"pageSize\": 10}'),(88,'导出岗位数据',NULL,1,0,0,13,'admin','2024-03-21 19:39:05.774',NULL,NULL,NULL,0,5,'{\"pageNo\": 1, \"pageSize\": 10}'),(89,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-21 21:28:29.213',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/aWNuWVpvKF8-dGgjfGU9Rl1wQAi5_Q4LRnscRGjM5y0HDvNcnMEK-DDA4Q==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-22-21 19:22:55\"}'),(90,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-26 14:25:43.132',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/diNILEw4NX0oXAwuNkQ5Rou9XxgdA6NInairh308GyppCj2YdRrSnHf0sw==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-20-26 11:20:50\"}'),(91,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-26 15:08:38.438',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/dF83ak04JTtjdElmX2k_I0zZDADNoQJJ9F-WKCdUwL4szYDhO6Y_BRjriA==\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-25-26 14:25:43\"}'),(92,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-26 16:47:06.667',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/KilTXFZ0LGUgTjw3MFYsRZT55q9PG_koAJLZ0PYDuppIfaYAgRGy8fDjLg==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-08-26 15:08:38\"}'),(93,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-28 13:12:44.999',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/QGlNQDQqZgtJPGM-Qlt2KMVB23GISV9lRp02mN_Z-6RJu8iSRQSGyOO36w==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-47-26 16:47:06\"}'),(94,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-28 13:34:05.387',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/QGlNQDQqZgtJPGM-Qlt2KMVB23GISV9lRp02mN_Z-6RJu8iSRQSGyOO36w==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-12-28 13:12:45\"}'),(95,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-28 15:21:14.524',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/aH5YWTw5clZoCy1cTVAmI4Zr0KMLssOvVijhwQrPNHoUpM4NvAI5z2GAbQ==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-34-28 13:34:06\"}'),(96,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-28 15:30:16.491',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/aH5YWTw5clZoCy1cTVAmI4Zr0KMLssOvVijhwQrPNHoUpM4NvAI5z2GAbQ==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-21-28 15:21:15\"}'),(97,'发送异地登录邮件',NULL,2,0,0,1,'系统管理员','2024-03-28 22:29:58.242',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/VGRSXGJMaGF6SX4-P2s1CvHcxecSFwZd9DQQ80sN80H7FDcGxVCMoA-sZA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-30-28 15:30:16\"}'),(98,'发送异地登录邮件',NULL,2,0,0,1,'系统管理员','2024-03-28 22:30:10.438',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/VGRSXGJMaGF6SX4-P2s1CvHcxecSFwZd9DQQ80sN80H7FDcGxVCMoA-sZA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-30-28 15:30:16\"}'),(99,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-28 22:30:58.082',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/VGRSXGJMaGF6SX4-P2s1CvHcxecSFwZd9DQQ80sN80H7FDcGxVCMoA-sZA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-30-28 15:30:16\"}'),(100,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-29 14:50:11.047',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/cCZ1QGhlWGBsC0tOfEFEQJA77PG3tZFN7OuvCTtLusMgGoodWiuVEAaVuA==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-30-28 22:30:58\"}'),(101,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-29 15:58:23.487',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/TSkgTDUhImVCSSFYKGY8O-vOr0sahqammzM0HfOvNY0KUUCoA2tqpsJgBw==\", \"cityName\": \"北京\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-50-29 14:50:11\"}'),(102,'发送异地登录邮件',NULL,2,0,0,13,'admin','2024-03-29 16:07:27.987',NULL,NULL,NULL,0,1,'{\"ip\": \"127.0.0.1\", \"email\": \"12lisu@163.com\", \"device\": \"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Aoyou/TSkgTDUhImVCSSFYKGY8O-vOr0sahqammzM0HfOvNY0KUUCoA2tqpsJgBw==\", \"cityName\": \"未知\", \"nickName\": \"超级管理员\", \"username\": \"admin\", \"loginTime\": \"2024-59-29 15:59:05\"}');
/*!40000 ALTER TABLE `common_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dept` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(30) NOT NULL COMMENT '名称',
  `pid` bigint NOT NULL COMMENT '上级部门',
  `valid_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '有效状态 1:有效 0:无效',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES (1,'苏三公司',0,1,13,'admin','2024-01-27 17:56:37.427',13,'admin','2024-01-27 18:10:32.693',0),(2,'苏三子公司',1,1,13,'admin','2024-01-27 18:22:54.920',NULL,NULL,NULL,0),(3,'测试部门',0,0,1,'系统管理员','2024-02-12 21:37:12.009',0,'string','2024-02-12 21:37:08.829',0),(4,'Java突击队',0,0,1,'系统管理员','2024-02-12 21:37:16.038',0,'string','2024-02-12 21:37:08.829',0),(5,'string',0,0,1,'系统管理员','2024-02-12 21:37:17.264',13,'admin','2024-03-29 14:54:13.680',1),(6,'string',0,0,1,'系统管理员','2024-02-12 21:37:18.205',13,'admin','2024-03-29 14:53:44.739',1),(7,'string',0,0,1,'系统管理员','2024-02-12 21:38:10.649',13,'admin','2024-03-29 14:53:44.739',1),(8,'string',0,0,1,'系统管理员','2024-02-12 21:41:32.719',13,'admin','2024-03-29 14:53:44.739',1),(9,'string',0,0,1,'系统管理员','2024-02-13 08:14:27.264',13,'admin','2024-03-29 14:53:44.739',1),(10,'string',0,0,1,'系统管理员','2024-02-13 08:17:12.929',13,'admin','2024-03-29 14:53:44.739',1),(11,'string',0,0,1,'系统管理员','2024-02-13 08:18:26.024',13,'admin','2024-03-29 14:54:05.585',1),(12,'string',0,0,1,'系统管理员','2024-02-13 08:18:54.864',13,'admin','2024-03-29 14:54:05.585',1),(13,'string',0,0,1,'系统管理员','2024-02-13 08:19:00.801',13,'admin','2024-03-29 14:53:58.286',1),(14,'string',0,0,1,'系统管理员','2024-02-13 08:21:17.784',13,'admin','2024-03-29 14:53:58.286',1),(15,'string',0,0,1,'系统管理员','2024-02-13 09:30:31.583',13,'admin','2024-03-29 14:53:58.286',1),(16,'测试',0,1,13,'admin','2024-02-24 21:38:09.350',13,'admin','2024-03-29 14:54:05.585',1),(17,'测试',0,1,13,'admin','2024-02-24 21:38:26.183',13,'admin','2024-03-29 14:54:05.585',1),(18,'测试',0,1,13,'admin','2024-02-24 21:40:48.696',13,'admin','2024-03-29 14:54:13.680',1),(19,'测试',0,1,13,'admin','2024-02-24 22:17:46.194',13,'admin','2024-03-29 14:54:13.680',1),(20,'测试',0,1,13,'admin','2024-02-24 22:25:46.450',13,'admin','2024-03-29 14:54:13.680',1),(21,'测试',0,1,13,'admin','2024-02-24 22:27:06.517',13,'admin','2024-03-29 14:54:13.680',1),(22,'市场部',0,1,13,'admin','2024-03-29 14:58:19.476',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict`
--

DROP TABLE IF EXISTS `sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` bigint NOT NULL DEFAULT '0' COMMENT '父字段ID',
  `dict_name` varchar(30) NOT NULL COMMENT '字典名称',
  `dict_description` varchar(30) NOT NULL COMMENT '字典描述',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict`
--

LOCK TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` VALUES (1,0,'valid_status','有效状态',13,'admin','2024-03-26 14:30:52.924',13,'admin','2024-03-26 15:46:38.598',0),(2,0,'delete_status','删除状态',13,'admin','2024-03-26 15:47:47.990',13,'admin','2024-03-26 14:31:07.856',0);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict_detail`
--

DROP TABLE IF EXISTS `sys_dict_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict_detail` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_id` bigint NOT NULL COMMENT '数据字典id',
  `value` varchar(30) NOT NULL COMMENT '值',
  `label` varchar(30) NOT NULL COMMENT '文本',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  `sort` int NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `dict_id_index` (`dict_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict_detail`
--

LOCK TABLES `sys_dict_detail` WRITE;
/*!40000 ALTER TABLE `sys_dict_detail` DISABLE KEYS */;
INSERT INTO `sys_dict_detail` VALUES (1,1,'1','有效',13,'admin','2024-03-26 15:19:35.540',13,'admin','2024-03-26 17:02:09.940',0,1),(2,1,'false','无效',13,'admin','2024-03-26 15:21:05.580',13,'admin','2024-03-26 15:46:09.920',1,2),(3,1,'0','无效',13,'admin','2024-03-26 15:46:22.370',13,'admin','2024-03-26 17:02:14.293',0,2),(4,1,'false','测试',13,'admin','2024-03-28 13:49:46.596',13,'admin','2024-03-28 14:14:41.086',1,3),(5,1,'0','测试',13,'admin','2024-03-28 15:21:41.833',13,'admin','2024-03-29 14:50:57.755',1,3);
/*!40000 ALTER TABLE `sys_dict_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_job`
--

DROP TABLE IF EXISTS `sys_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_job` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(30) NOT NULL COMMENT '岗位名称',
  `sort` int NOT NULL COMMENT '岗位排序',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `valid_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '有效状态 1:有效 0:无效',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_dept_id` (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='岗位';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_job`
--

LOCK TABLES `sys_job` WRITE;
/*!40000 ALTER TABLE `sys_job` DISABLE KEYS */;
INSERT INTO `sys_job` VALUES (1,'Java初级开发工程师',2,NULL,1,13,'admin','2024-03-21 19:09:52.682',13,'admin','2024-03-21 19:23:04.860',1),(2,'Java中级工程师',2,NULL,0,13,'admin','2024-03-21 19:11:39.184',13,'admin','2024-03-21 19:26:59.810',0),(3,'Java高级工程师',2,NULL,1,13,'admin','2024-03-21 19:11:51.237',13,'admin','2024-03-21 19:26:52.086',0),(4,'Java初级工程师',1,NULL,1,13,'admin','2024-03-21 19:23:20.884',13,'admin','2024-03-21 19:23:28.657',1),(5,'Java初级工程师',2,NULL,1,13,'admin','2024-03-21 21:31:02.628',13,'admin','2024-03-21 21:32:28.460',1),(6,'Java初级工程师',3,NULL,1,13,'admin','2024-03-29 14:53:14.917',13,'admin','2024-03-29 14:58:35.411',0),(7,'Java架构师',3,NULL,1,13,'admin','2024-03-29 16:08:12.511',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `sys_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `pid` bigint NOT NULL COMMENT '上级菜单ID',
  `sort` int NOT NULL COMMENT '排序',
  `icon` varchar(30) DEFAULT NULL COMMENT '图标',
  `path` varchar(255) DEFAULT NULL COMMENT '路由',
  `hidden` tinyint(1) DEFAULT '0' COMMENT '是否隐藏',
  `is_link` tinyint(1) DEFAULT '0' COMMENT '是否外链 1：是 0：否',
  `type` int DEFAULT NULL COMMENT '类型 1：目录 2：菜单 3：按钮',
  `permission` varchar(255) DEFAULT NULL COMMENT '功能权限',
  `url` varchar(30) DEFAULT NULL COMMENT 'url地址',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  `component` varchar(20) DEFAULT NULL COMMENT '组件',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_pid` (`pid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1,'系统管理',0,1,'system','/system',0,0,1,NULL,NULL,1,'管理员','2024-01-17 10:58:03.382',13,'admin','2024-01-22 18:45:14.932',0,'Layout'),(2,'菜单管理',1,1,'menu','/menu',0,0,1,NULL,NULL,1,'管理员','2024-01-17 10:58:03.382',NULL,NULL,NULL,0,'system/menu/index'),(3,'部门管理',1,2,'dept','/dept',0,0,1,NULL,NULL,1,'管理员','2024-01-17 10:58:03.382',13,'admin','2024-01-22 18:46:04.376',0,'system/dept/index'),(4,'用户管理',1,3,'user','/user',0,0,1,NULL,NULL,1,'管理员','2024-01-17 10:58:03.382',NULL,NULL,NULL,0,'system/user/index'),(5,'角色管理',0,2,'role','/role',0,0,0,NULL,NULL,13,'admin',NULL,13,'admin','2024-01-23 16:54:12.412',0,NULL),(6,'test',0,999,'doc','/test',0,0,0,NULL,NULL,13,'admin',NULL,NULL,NULL,NULL,0,NULL),(7,'岗位管理',1,999,'Steve-Jobs','/job',0,0,1,NULL,NULL,13,'admin','2024-03-21 18:04:20.047',NULL,NULL,NULL,0,'system/job/index'),(8,'数据字典管理',1,999,'dictionary','/dict',0,0,1,NULL,NULL,1,'系统管理员','2024-03-26 11:17:01.435',13,'admin','2024-03-26 11:28:52.964',0,'system/dict/index');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(30) NOT NULL COMMENT '名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `data_scope` varchar(255) DEFAULT NULL COMMENT '数据权限',
  `level` int DEFAULT NULL COMMENT '角色级别',
  `permission` varchar(255) DEFAULT NULL COMMENT '功能权限',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'超级管理员',NULL,'全部',1,'admin',1,'超级管理员','2024-01-22 16:08:40.334',1,'超级管理员','2024-01-22 16:08:40.334',0);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_dept`
--

DROP TABLE IF EXISTS `sys_role_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_dept` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` bigint NOT NULL,
  `dept_id` bigint NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_role_id` (`role_id`) USING BTREE,
  KEY `index_dept_id` (`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色部门关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_dept`
--

LOCK TABLES `sys_role_dept` WRITE;
/*!40000 ALTER TABLE `sys_role_dept` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_role_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_role_id` (`role_id`) USING BTREE,
  KEY `index_menu_id` (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色菜单关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `avatar_id` bigint DEFAULT NULL COMMENT '头像',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `password` varchar(60) DEFAULT NULL COMMENT '密码',
  `user_name` varchar(30) DEFAULT NULL COMMENT '用户名',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `job_id` bigint DEFAULT NULL COMMENT '岗位ID',
  `last_change_password_time` datetime(3) DEFAULT NULL COMMENT '最后修改密码的日期',
  `nick_name` varchar(30) DEFAULT NULL COMMENT '别名',
  `sex` tinyint DEFAULT NULL COMMENT '性别 1：男 2：女',
  `valid_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '有效状态 1:有效 0:无效',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  `last_login_city` varchar(10) DEFAULT NULL COMMENT '最后登录城市',
  `last_login_time` datetime(3) DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uk_user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,NULL,NULL,NULL,'苏三说技术',NULL,NULL,NULL,NULL,NULL,NULL,1,1,'java突击队',NULL,NULL,NULL,'2024-01-07 09:59:53.148',1,NULL,NULL),(2,0,'string','string','string',0,'string',0,'2024-01-09 13:58:53.047','string',0,0,0,'string','2024-01-09 13:58:53.047',0,'string','2024-01-09 13:58:53.047',0,NULL,NULL),(4,0,'string','string','string2',0,'string',0,'2024-01-09 13:58:53.047','string',0,1,0,'string','2024-01-09 13:58:53.047',0,'string','2024-01-09 13:58:53.047',0,NULL,NULL),(6,0,'string','string','string3',0,'string',0,'2024-01-09 14:27:31.675','string',0,0,0,'string','2024-01-09 14:27:31.675',0,'string','2024-01-09 14:27:31.675',0,NULL,NULL),(13,1,'12lisu@163.com','$2a$10$iTbdKnF58RWY5GC6arDuM.oDHEjil/Y9vQrQlhQ.FXpWCZQ0GSLS.','admin',1,'18200256777',0,'2024-01-22 16:20:58.138','超级管理员',1,0,0,'string','2024-01-22 16:20:58.138',13,'admin','2024-03-29 16:07:49.825',0,'未知','2024-03-29 16:07:37.973');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_avatar`
--

DROP TABLE IF EXISTS `sys_user_avatar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_avatar` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL COMMENT '文件名',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `file_size` varchar(10) DEFAULT NULL COMMENT '大小',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除 1：已删除 0：未删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户头像';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_avatar`
--

LOCK TABLES `sys_user_avatar` WRITE;
/*!40000 ALTER TABLE `sys_user_avatar` DISABLE KEYS */;
INSERT INTO `sys_user_avatar` VALUES (1,'test.png','/test.png','1KB',1,'管理员','2024-01-22 17:39:58.161',1,'管理员','2024-01-22 17:39:58.161',0);
/*!40000 ALTER TABLE `sys_user_avatar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `index_user_id` (`user_id`) USING BTREE,
  KEY `index_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户角色关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,13,1);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_name` varchar(30) DEFAULT NULL COMMENT '用户名',
  `create_user_id` bigint NOT NULL COMMENT '创建人ID',
  `create_user_name` varchar(30) NOT NULL COMMENT '创建人名称',
  `create_time` datetime(3) NOT NULL COMMENT '创建日期',
  `update_user_id` bigint DEFAULT NULL COMMENT '修改人ID',
  `update_user_name` varchar(30) DEFAULT NULL COMMENT '修改人名称',
  `update_time` datetime(3) DEFAULT NULL COMMENT '修改日期',
  `is_del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'苏三说技术',1,'苏三','2024-01-04 17:14:19.469',NULL,NULL,'2024-01-07 09:59:44.551',1),(2,'java突击队',2,'java突击队','2024-01-07 09:49:58.079',NULL,NULL,'2024-01-07 09:49:58.079',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-29 16:15:15
