package cn.net.susan.job;

import cn.net.susan.enums.JobResult;
import cn.net.susan.service.es.SyncProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static cn.net.susan.enums.JobResult.SUCCESS;

/**
 * 同步商品数据ES
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/14 下午4:44
 */
@Slf4j
@Component
public class SyncProductToEsJob extends BaseJob {

    @Autowired
    private SyncProductService syncProductService;

    @Override
    public JobResult doRun(String params) {
        syncProductService.syncProductToES();
        return SUCCESS;
    }
}
