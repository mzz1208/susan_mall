package cn.net.susan.job;

import cn.net.susan.enums.JobResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 苏三
 * @date 2024/8/6 上午10:48
 */
@Component
@Slf4j
public class TestJob extends BaseJob {

    @Override
    public JobResult doRun(String params) {
        log.info("==TestJob开始执行==");
        return null;
    }
}
