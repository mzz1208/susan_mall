package cn.net.susan.quartz;

import cn.net.susan.entity.common.CommonJobConditionEntity;
import cn.net.susan.entity.common.CommonJobEntity;
import cn.net.susan.mapper.common.CommonJobMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 项目启动的时候加载
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/5/1 上午10:47
 */
@Slf4j
@Component
public class QuartzJobRunner implements ApplicationRunner {

    @Autowired
    private CommonJobMapper commonJobMapper;
    @Autowired
    private QuartzManage quartzManage;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("--------------------开始注入定时任务---------------------");
        CommonJobConditionEntity commonJobConditionEntity = new CommonJobConditionEntity();
        commonJobConditionEntity.setPauseStatus(0);
        commonJobConditionEntity.setPageSize(0);
        List<CommonJobEntity> commonJobEntities = commonJobMapper.searchByCondition(commonJobConditionEntity);

        if (CollectionUtils.isNotEmpty(commonJobEntities)) {
            for (CommonJobEntity commonJobEntity : commonJobEntities) {
                quartzManage.addJob(commonJobEntity);
            }
        }
        log.info("--------------------定时任务注入完成，注入定时的数量是：{}---------------------", commonJobEntities.size());
    }
}
