package cn.net.susan.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 请求条件实体
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/25 下午4:43
 */
@Data
public class RequestConditionEntity extends RequestPageEntity {


    /**
     * 创建日期范围
     */
    @ApiModelProperty("创建日期范围")
    private List<String> betweenTime;

    /**
     * 创建开始时间
     */
    private String createBeginTime;

    /**
     * 创建结束时间
     */
    private String createEndTime;

    /**
     * 自定义excel表头列表
     */
    private List<String> customizeColumnNameList;

    /**
     * 查询条件
     */
    private String blurry;
}
